# README #

Please ensure that the raspberry pi is up and running. Follow the [docs](https://www.raspberrypi.org/documentation/setup/) here.

### Next Steps ###

* Enable raspi cam from raspi-config
* Install [raspicam-opencv libraries](https://github.com/cedricve/raspicam) to finish the setup

### Running ###

Just clone the repo and run the python file. The code will take a snapshot and save it with the faces marked.

### Contribution guidelines ###

* Writing tests
* Code review